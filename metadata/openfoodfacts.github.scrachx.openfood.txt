Categories:Sports & Health
License:Apache2
Web Site:https://github.com/openfoodfacts/OpenFoodFacts-androidApp/blob/HEAD/README.md
Source Code:https://github.com/openfoodfacts/OpenFoodFacts-androidApp
Issue Tracker:https://github.com/openfoodfacts/OpenFoodFacts-androidApp/issues

Auto Name:OpenFood
Summary:Look up ingredients, allergens, nutrition facts
Description:
Database of food products with ingredients, allergens, nutrition facts and all
the tidbits of information we can find on product labels.
.

Repo Type:git
Repo:https://github.com/openfoodfacts/OpenFoodFacts-androidApp

Build:0.1.5,6
    disable=https://github.com/openfoodfacts/OpenFoodFacts-androidApp/issues/25
    commit=0.1.5
    subdir=app
    gradle=yes
    prebuild=sed -i -e "/support-v4/s/21.1.1/21.0.3/g" -e "/support-v4/acompile('com.github.afollestad.material-dialogs:commons:0.8.5.7@aar') {\ntransitive = true\n}\n" -e '/com.afollestad:material-dialogs/d' build.gradle && \
        sed -i -e '/jcenter/amaven { url "https://jitpack.io" }' ../build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1.5
Current Version Code:6
