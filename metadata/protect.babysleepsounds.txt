Categories:Multimedia
License:GPLv3+
Web Site:
Source Code:https://github.com/brarcher/baby-sleep-sounds
Issue Tracker:https://github.com/brarcher/baby-sleep-sounds/issues

Auto Name:Baby Sleep Sounds
Summary:Play sounds to help babies sleep
Description:
Baby Sleep Sounds will play a variety of sounds to help a baby sleep soundly.
.

Repo Type:git
Repo:https://github.com/brarcher/baby-sleep-sounds

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.2
Current Version Code:2
