Categories:Internet
License:GPLv3+
Web Site:https://github.com/scoute-dich/Weather/blob/HEAD/README.md
Source Code:https://github.com/scoute-dich/Weather
Issue Tracker:https://github.com/scoute-dich/Weather/issues
Changelog:https://github.com/scoute-dich/Weather/blob/HEAD/CHANGELOG.md

Auto Name:Weather
Summary:Get weather information from several webservices
Description:
Show weather information from wetterdienst.de, meteoblue.com and the DWD.

Features:

* capture websites for offline use
* share screenshot of whole webview
* five predefined locations
* search and save other locations as bookmarks
* usefull weather informations (satelitt, rain radar, topic of the day, ...)

[https://github.com/scoute-dich/Weather/blob/master/SCREENSHOTS.md Screenshots]
.

Repo Type:git
Repo:https://github.com/scoute-dich/Weather

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.1,2
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.2,4
    commit=v1.2
    subdir=app
    gradle=yes

Build:2.1,6
    commit=v2.1
    subdir=app
    gradle=yes

Build:2.1.2,9
    commit=v2.1.2
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.1.2
Current Version Code:9
